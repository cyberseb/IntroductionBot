# this script uses the Mastodon API to look for people that used the #introduction hashtag lately and posts a summary.

# library for date and time functions
import datetime
# library for timezones
import pytz
# library for access to Mastodon API
from mastodon import Mastodon
# library for serialization to store variable on disk
import pickle


# creates login credentials (only needs to be run one time)
def create_creds(instance):
    Mastodon.create_app(
        'pytooterapp',
        api_base_url='https://' + instance,
        to_file='pytooter_clientcred.secret'
    )
    mastodon = Mastodon(
        client_id='pytooter_clientcred.secret',
        api_base_url='https://' + instance
    )
    # !!! Here is where you need to fill in the username + password in clear-text !!!
    mastodon.log_in(
        'user-email@example.com',
        'secret-password',
        to_file='pytooter_usercred.secret'
    )


# returns the last query timestamp from a file
def get_last_query(filename):
    # load last query timestamp from local file...
    infile = open(filename, 'rb')
    last_query_timestamp = pickle.load(infile)
    infile.close()
    return last_query_timestamp


# updates the last query timestamp in a file
def update_last_query(filename, new_query_timestamp):
    # update last query timestamp in the file
    outfile = open(filename, 'wb')
    pickle.dump(new_query_timestamp, outfile)
    outfile.close()


# returns introduction posts in an array
def discover(instance, last_query_timestamp):
    # This array will take the found introductions
    found_intros = []
    # We keep track of timestamps, so we don't post the same twice
    print('Last timestamp: ' + str(last_query_timestamp))
    new_query_timestamp = pytz.utc.localize(datetime.datetime.utcnow())
    print('New timestamp: ' + str(new_query_timestamp))

    # connect to Instance API
    mstdn = Mastodon(
        access_token='pytooter_usercred.secret',
        api_base_url='https://' + instance
    )
    # Discover toots with #introduction hashtag
    toots = mstdn.timeline_hashtag('introduction')
    for toot in toots:
        if toot['created_at'] >= last_query_timestamp:
            found_intros.append(toot)
    # Discover toots with #introductions hashtag
    toots2 = mstdn.timeline_hashtag('introductions')
    for toot2 in toots2:
        if toot2['created_at'] >= last_query_timestamp:
            found_intros.append(toot2)
    # Discover toots with #intro hashtag
    toots3 = mstdn.timeline_hashtag('intro')
    for toot3 in toots3:
        if toot3['created_at'] >= last_query_timestamp:
            found_intros.append(toot3)

    # write last query timestamp to a file
    update_last_query('lastQueryTimestamp', new_query_timestamp)

    # return all found users in an array
    return found_intros


# Toots the new user with a Welcome message
def toot_summary(instance, toots):
    users = ''
    for toot in toots:
        users = users + '@' + toot['account']['acct'] + '\n'

    # connect to instance
    mstdn = Mastodon(
        access_token='pytooter_usercred.secret',
        api_base_url='https://' + instance
    )
    mstdn.toot('Here are the Fediverse Introductions of the last 7 days:\n\n' + users + '\nGo and say Hello!')


# main loop
if __name__ == '__main__':
    # Put in your instance name here:
    mastodon_instance = 'ioc.exchange'
    # create_creds(mastodon_instance)

    # Let's see what new users we can find...
    # td = datetime.timedelta(7)
    # intros = discover(mastodon_instance, get_last_query('lastQueryTimestamp') - td)
    intros = discover(mastodon_instance, get_last_query('lastQueryTimestamp'))
    for intro in intros:
        print(intro)

    # Send the Summary Toot
    toot_summary(mastodon_instance, intros)
