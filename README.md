# IntroductionBot

A Mastodon bot that looks for introduction toots and sends a summary toot to foster engagement.

## Dependencies

Use PIP to install the two external libraries:

    pip3 install Mastodon.py
            

## How do I run it..?

I run it via crontab every Friday:

0 9 * * FRI     run_introbot.sh


Here is the shell script (run_introbot.sh) I use to run this Python script via crontab:

    cd /home/mastodon/IntroductionBot
    python3 introbot.py >> introbot.log


The Log output for no introduction toot found, looks like this:

    $ cat GreeterBot/greeter.log 
    Last timestamp: 2021-07-05 03:15:09.117215
    New timestamp: 2021-07-05 03:20:36.146875
    []
    Last timestamp: 2021-07-05 03:20:36.146875
    New timestamp: 2021-07-05 03:25:02.082494
    []
